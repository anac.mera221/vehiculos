/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vehiculos;

/**
 *
 * @author sistemas
 * @class AutoVolador
 * @version 1.0
 * Clase de un auto volador que hereda dela clase carro para su implementación
 */
public class AutoVolador extends Carro {
    public boolean esta_Volando=false;

    public AutoVolador(String color, byte modelo, String marca) {
        super(color, modelo, marca);
    }

    public boolean isEsta_Volando() {
        return esta_Volando;
    }

    public void setEsta_Volando(boolean esta_Volando) {
        this.esta_Volando = esta_Volando;
    }

    public AutoVolador(char placa, boolean pesado) {
        super(placa, pesado);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public char getPlaca() {
        return placa;
    }

    public void setPlaca(char placa) {
        this.placa = placa;
    }

    public byte getModelo() {
        return modelo;
    }

    public void setModelo(byte modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public boolean isPesado() {
        return pesado;
    }

    public void setPesado(boolean pesado) {
        this.pesado = pesado;
    }

    /**
     * 
     */
    public void Volar(boolean estado){
        this.esta_Volando=estado;
        
    }
    }

 

   

    
   
    
    
}
